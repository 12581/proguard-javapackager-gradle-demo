package com.example.demo;

import com.example.demo.sub.TestComponent2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootApplication
@EntityScan(basePackages = {"com.example.demo"})
public class DemoApplication {
	private static final Logger log = LoggerFactory.getLogger(DemoApplication.class);

	public static void main(String[] args) {
		final ConfigurableApplicationContext context = SpringApplication.run(DemoApplication.class, args);
		// SpringUtil.setApplicationContext(run);
		TestComponent ms   = context.getBean(TestComponent.class);
		TestComponent2 ms2 = context.getBean(TestComponent2.class);

		System.out.println("The answer is " + ms.getAnswer() + " " + ms2.getMessage());

		log.info("test");
		/*AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.scan("com.example.demo");
		context.refresh();

		TestComponent ms   = context.getBean(TestComponent.class);
		TestComponent2 ms2 = context.getBean(TestComponent2.class);

		System.out.println("The answer is " + ms.getAnswer() + " " + ms2.getMessage());
		context.close();*/
	}

}
